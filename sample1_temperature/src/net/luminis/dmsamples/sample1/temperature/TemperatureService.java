package net.luminis.dmsamples.sample1.temperature;

public interface TemperatureService {

    /**
     * @return   temperature in degrees Celcius
     */
    public int getTemperature();
}
