package net.luminis.dmsamples.sample2.temperature.impl;

import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * Null-object replacement for the LogService; just prints log messages to std out (poor man's logging)
 */
public class SysoutPrinter implements LogService {

    @Override
    public void log(int level, String message) {
        System.out.println(message);
    }

    @Override
    public void log(int level, String message, Throwable exception) {
        System.out.println(message);
    }

    @Override
    public void log(ServiceReference sr, int level, String message) {
        System.out.println(message);
    }

    @Override
    public void log(ServiceReference sr, int level, String message, Throwable exception) {
        System.out.println(message);
    }
}
