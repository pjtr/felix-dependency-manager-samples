package net.luminis.dmsamples.sample1.display;

import net.luminis.dmsamples.sample1.temperature.TemperatureService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Activator implements BundleActivator {

    private JFrame window;
    private BundleContext bundleContext;

    @Override
    public void start(final BundleContext context) {
        bundleContext = context;
        createUI();
    }

    @Override
    public void stop(BundleContext context) {
        window.dispose();
    }

    private void createUI() {
        final String empty = " ";

        JPanel panel = new JPanel();
        JButton refreshButton = new JButton("refresh");
        panel.add(refreshButton);

        final JLabel contentLabel = new JLabel(empty);
        contentLabel.setPreferredSize(new Dimension(80, contentLabel.getPreferredSize().height));
        contentLabel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.DARK_GRAY),
               BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        panel.add(contentLabel);

        window = new JFrame("temperature display");
        window.getContentPane().add(panel);

        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ServiceReference<TemperatureService> ref = bundleContext.getServiceReference(TemperatureService.class);
                if (ref != null) {
                    TemperatureService service = bundleContext.getService(ref);
                    int temperature = service.getTemperature();
                    contentLabel.setText("" + temperature + " \u2103");
                    bundleContext.ungetService(ref);
                }
                else {
                    contentLabel.setText(empty);
                }
            }
        });

        window.pack();
        window.setVisible(true);
    }
}
