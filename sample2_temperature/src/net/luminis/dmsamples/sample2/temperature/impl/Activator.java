package net.luminis.dmsamples.sample2.temperature.impl;

import net.luminis.dmsamples.sample2.temperature.TemperatureService;
import net.luminis.dmsamples.sample1.sensor.SensorService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext bundleContext, DependencyManager dependencyManager) {

        Component component = dependencyManager.createComponent()
                .setInterface(TemperatureService.class.getName(), null)
                .setImplementation(TemperatureServiceImpl.class)
                .add(createServiceDependency().setService(SensorService.class).setRequired(true));

        component.add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false)
        //        .setDefaultImplementation(new SysoutPrinter())   // Set custom "null object"-implementation
        );

        dependencyManager.add(component);
    }

    @Override
    public void destroy(BundleContext bundleContext, DependencyManager dependencyManager) {
    }
}
