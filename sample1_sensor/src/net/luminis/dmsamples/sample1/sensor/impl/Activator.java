package net.luminis.dmsamples.sample1.sensor.impl;

import net.luminis.dmsamples.sample1.sensor.SensorService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Dictionary;
import java.util.Properties;

public class Activator implements BundleActivator {

    ServiceRegistration<SensorService> registration;

    @Override
    public void start(BundleContext context) {
        Dictionary serviceProps = new Properties();
        serviceProps.put("type", "temperature");
        registration = context.registerService(SensorService.class, new SensorSimulator(), serviceProps);
    }

    @Override
    public void stop(BundleContext context) {
        // Not really necessary (if we don't unregister the service, the OSGi framework will do it for us), but let's make it explicit
        registration.unregister();
    }

    /**
     * Simulates a sensor.
     */
    private static class SensorSimulator implements SensorService {

        int temperature = 0;

        /**
         * Returns the measured value of the sensor.
         */
        public int getMeasurement() {
            int current = temperature;
            if (temperature < 500) {
                temperature += 10;
            }
            else {
                temperature = -100;
            }
            return current;
        }
    }
}
