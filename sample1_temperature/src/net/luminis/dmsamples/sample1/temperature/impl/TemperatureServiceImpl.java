package net.luminis.dmsamples.sample1.temperature.impl;

import net.luminis.dmsamples.sample1.temperature.TemperatureService;
import net.luminis.dmsamples.sample1.sensor.SensorService;

public class TemperatureServiceImpl implements TemperatureService {

    // Injected by DependencyManager, see Activator.init()
    private volatile SensorService sensor;

    public int getTemperature() {
        int measuredValue = sensor.getMeasurement();
        int degreesCelcius = (measuredValue / 10) - 20;
        return degreesCelcius;
    }
}
