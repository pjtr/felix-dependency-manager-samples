
# Apache Felix Dependency Manager sample code

In this git repository, you'll find sample source code related to a series of [articles](http://arnhem.luminis.eu/blog/osgi) on the Apache Felix [Dependency Manager](http://felix.apache.org/site/apache-felix-dependency-manager.html). 

## Building

All samples can be build with [gradle](http://www.gradle.org/). If you have gradle installed, the command `gradle assemble` will do the job. 

## Download

For your conveninece, binary (pre-build) bundles are available too. Just go to [downloads](https://bitbucket.org/pjtr/felix-dependency-manager-samples/downloads) and get what you need.

## Deployment

Directions for how to deploy and run the sample bundles in the Apache Felix OSGi container, can be found at the end of the [article](http://arnhem.luminis.eu/introduction-to-the-apache-felix-dependency-manager/).

## Feedback

All feedback, including question, is welcome; just post comments to the [article](http://arnhem.luminis.eu/introduction-to-the-apache-felix-dependency-manager/).
