package net.luminis.dmsamples.sample2.temperature.impl;

import net.luminis.dmsamples.sample2.temperature.TemperatureService;
import net.luminis.dmsamples.sample1.sensor.SensorService;

import org.osgi.service.log.LogService;

public class TemperatureServiceImpl implements TemperatureService {

    // Injected by DependencyManager, see Activator.init()
    private volatile SensorService sensor;

    // Injected by DependencyManager, see Activator.init()
    private volatile LogService logger;

    public int getTemperature() {
        // System.out.println("The value of the 'logger' field is now: " + logger + " and its type is " + logger.getClass());
        logger.log(LogService.LOG_DEBUG, "getTemperature is called");

        int measuredValue = sensor.getMeasurement();
        int degreesCelcius = (measuredValue / 10) - 20;
        return degreesCelcius;
    }
}
