package net.luminis.dmsamples.sample2.temperature;

public interface TemperatureService {

    /**
     * @return   temperature in degrees Celcius
     */
    public int getTemperature();
}
