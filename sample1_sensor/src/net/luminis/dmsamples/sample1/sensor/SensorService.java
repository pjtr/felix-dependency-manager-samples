package net.luminis.dmsamples.sample1.sensor;

public interface SensorService {

    /**
     * Returns the measured value of the sensor.
     */
    int getMeasurement();
}
